FROM php:7.1.8-cli
RUN apt-get update && apt-get install -y \
    git \
    python

RUN git clone https://github.com/edenhill/librdkafka.git
WORKDIR "librdkafka"
RUN ./configure && make && make install

RUN pecl install rdkafka && docker-php-ext-enable rdkafka

RUN pecl config-set php_ini /usr/local/etc/php/php.ini
RUN mkdir /app
COPY . /app/
WORKDIR /app

ENTRYPOINT ["php","-f","index.php"]
