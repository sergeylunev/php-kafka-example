<?php
$rk = new \RdKafka\Producer();
$rk->setLogLevel(LOG_DEBUG);
$rk->addBrokers('127.0.0.1');
$topic = $rk->newTopic("halo");

$message = [
  'header' => [
    'message_uuid' => 'mUUID',
    'action' => 'command',
    'category' => 'admin_command',
  ],
  'body' => [
    'serviceUUID' => 'config'
  ]
];

$topic->produce(RD_KAFKA_PARTITION_UA, 0, json_encode($message));

