<?php
$rk = new \RdKafka\Producer();
$rk->setLogLevel(LOG_DEBUG);
$rk->addBrokers('127.0.0.1');
$topic = $rk->newTopic("set_config");

$message = [
  'header' => [
    'message_uuid' => 'mUUID',
    'action' => 'command',
    'category' => 'admin_command',
  ],
  'body' => [
    'config' => [
      [
        'key' => 'test',
        'value' => 1
      ],
      [
        'key' => 'test',
        'value' => 2
      ], 
      [
        'key' => 'second',
        'value' => 3
      ], 
    ]
  ]
];

$topic->produce(RD_KAFKA_PARTITION_UA, 0, json_encode($message));

